#include "track.h"
#include <iostream>
#include <chrono>

Track::Track():
  id(0),
  name(""),
  path(""),
  duration(0),
  format(),
  genre(),
  subgenre(),
  polyphony()
{}

/*
// Constructeur avec paramètres pour l'artiste et l'album de Track
Track::Track(unsigned int _id, std::string _name, std::string _way, std::chrono::duration<int> _duration, Artist _artist, Album _album, Format _format, Genre _genre, Subgenre _subgenre, Polyphony _polyphony):
  id(_id),
  name(_name),
  way(_way),
  duration(_duration),
  artist(_artist),
  album(_album),
  format(_format),
  genre(_genre),
  subgenre(_subgenre),
  polyphony(_polyphony)
{}
*/


// Constructeur avec paramètres pour plusieurs artistes et plusieurs albums
Track::Track(unsigned int _id, std::string _name, std::string _path, std::chrono::duration<int> _duration, std::vector<Artist> _artists, std::vector<Album> _albums, Format _format, Genre _genre, Subgenre _subgenre, Polyphony _polyphony):
  id(_id),
  name(_name),
  path(_path),
  duration(_duration),
  artists(_artists),
  albums(_albums),
  format(_format),
  genre(_genre),
  subgenre(_subgenre),
  polyphony(_polyphony)
{}

Track::~Track() {}

unsigned int Track::getId()
{
  return id;
}

std::chrono::duration<int> Track::getDuration()
{
  return duration;
}

std::string Track::getName()
{
  return name;
}

std::string Track::getPath()
{
  return path;
}

std::vector<Artist> Track::getArtists()
{
  return artists;
}

std::vector<Album> Track::getAlbums()
{
  return albums;
}

/*
Artist Track::getArtist()
{
  return artist;
}

Album Track::getAlbum()
{
  return album;
}
*/
Format Track::getFormat()
{
  return format;
}

Genre Track::getGenre()
{
  return genre;
}

Subgenre Track::getSubgenre()
{
  return subgenre;
}

Polyphony Track::getPolyphony()
{
  return polyphony;
}


void Track::setDuration(std::chrono::duration<int> _duration)
{
  duration = _duration;
}

void Track::setName(std::string _name)
{
  name = _name;
}

void Track::setPath(std::string _path)
{
  path = _path;
}

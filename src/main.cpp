#include <libpq-fe.h>
#include <iostream>
#include "cli.h"
int main()
{
  int code_retour = 0;
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 user=a.bouchet dbname=Cours  password=P@ssword";
  PGPing ping = PQping(info_connexion);

  if(ping == PQPING_OK) {
    PGconn *connexion = PQconnectdb(info_connexion);
    std::cout << "C'est OK" << std::endl;
    if(PQstatus(connexion) != CONNECTION_OK) {
      std::cerr << "Erreur de Connexion" << std::endl;
      code_retour = 1;
    }
  }
  else {
    std::cerr << "Serveur non accessible" << std::endl;
    code_retour = 1;
  }
  return code_retour;
}

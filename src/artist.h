#ifndef ARTIST_H
#define ARTIST_H
#include <iostream>


/**
 * \class Artist
 * \brief Definition of the class Artist
 */
class Artist
{
 public:

  /**
   * \brief Default constructor of Artist
   */
  Artist();

  /**
   * \brief Constructor with parameters
   * \param[in] id the identification number of an artist
   * \param[in] name the name of an artist
   */
  Artist(unsigned int, std::string);

  /**
   * \brief Default destructor of Artist
   */
  ~Artist();

  /**
   * \brief Lets you know the id of an artist
   * \return unsigned int representing the id of an artist
   */
  unsigned int getId();

  /**
   * \brief Lets you know the name of an artist
   * \return std::string representing the name of an artist
   */
  std::string getName();

  /**
   * \brief to set the name of an artist
   */
  void setName(std::string);

 private:
  unsigned int id; ///< Attribute defining the id of the artist
  std::string name; ///< Attribute defining the name of the artist
};

#endif // ARTIST_H

#include "format.h"
#include <iostream>


Format::Format():
  Format(0, "")
{}

Format::Format(unsigned int _id, std::string _type):
  id(_id),
  type(_type)
{}

Format::~Format() {}

unsigned int Format::getId()
{
  return id;
}

std::string Format::getType()
{
  return type;
}

void Format::setType(std::string _type)
{
  type = _type;
}

#include "playlist.h"
#include <fstream>

Playlist::Playlist():
  id(0),
  title(""),
  duration(0)
{}

Playlist::Playlist(unsigned int _id, std::string _title, std::chrono::duration<int> _duration, std::vector<Track> _tracks):
  id(_id),
  title(_title),
  duration(_duration),
  tracks(_tracks)
{}

Playlist::~Playlist()
{}

unsigned int Playlist::getId()
{
  return id;
}

std::chrono::duration<int> Playlist::getDuration()
{
  return duration;
}

void Playlist::setDuration(std::chrono::duration<int> _duration)
{
  duration = _duration;
}

void Playlist::addTrack(Track t)
{
  tracks.push_back(t);
}

void Playlist::writeM3U()
{
  std::string filename = title + ".m3u";
  {
    std::ofstream m3u_stream(filename, std::ios::out);

    if(m3u_stream.is_open())
    {
      for(Track current_track : tracks)
      {
        m3u_stream << current_track.getPath() << std::endl;
      }
    }
  }
}

void Playlist::writeXSPF()
{}

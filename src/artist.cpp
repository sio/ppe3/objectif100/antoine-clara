#include "artist.h"
#include <iostream>

Artist::Artist():
  id(0),
  name("")
{}


Artist::Artist(unsigned int _id, std::string _name):
  id(_id),
  name(_name)
{}

Artist::~Artist()
{}


unsigned int Artist::getId()
{
  return id;
}

std::string Artist::getName()
{
  return name;
}

void Artist::setName(std::string _name)
{
  name = _name;
}

